module Todo
  # A way to put all constant in one place :)
  module CONSTANTS
    # buttons_to helper
    BUTTONS = {
      add: { icon: :plus, btn: 'btn-primary' },
      edit: { icon: :pencil, btn: 'btn-default' },
      show: { icon: :info, btn: 'btn-default' },
      back: { icon: 'arrow-left', btn: 'btn-default' },
      finish: { icon: :check, btn: 'btn-default' }
    }.freeze

    FLASH_MESSAGES = {
      notice: { icon: 'info', body: 'success' },
      alert: { icon: 'times', body: 'danger' }
    }.freeze

  end
end
