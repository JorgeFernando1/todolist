Rails.application.routes.draw do
  get 'log_in' => 'sessions#new', as: :log_in
  get 'log_out' => 'sessions#destroy', as: :log_out

  get 'sign_up' => 'users#new', as: :sign_up

  get 'welcome' => 'home#index', as: :welcome
  root 'home#index'

  resources :users
  resources :to_dos
  resources :sessions
  get 'to_dos/:id/finish', to: 'to_dos#finish', as: :finish_to_do
end
