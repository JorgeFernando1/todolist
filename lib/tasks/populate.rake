namespace :db do
  task populate: :environment do
    user = User.create(email: 'user@user.com', password: 'user',
                       password_confirmation: 'user')

    100.times do
      FactoryGirl.create(:to_do, user: user)
    end
  end
end
