class HomeController < ApplicationController
  skip_before_filter :verify_logged_in, only: [:index]

  def index
    if current_user
      render 'welcome_user'
    else
      render 'welcome_stranger'
    end
  end
end
