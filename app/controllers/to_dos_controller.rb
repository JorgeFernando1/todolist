class ToDosController < ApplicationController
  before_action :set_to_do, only: [:edit, :update, :destroy, :finish]

  def index
    collection
    respond_with @to_dos
  end

  def new
    @to_do = ToDo.new
  end

  def create
    @to_do = ToDo.new(to_do_params)
    @to_do.user = current_user
    @to_do.save
    respond_with(@to_do, location: -> { to_dos_path })
  end

  def update
    @to_do.update(to_do_params)
    respond_with(@to_do, location: -> { to_dos_path })
  end

  def finish
    @to_do.status = :finished
    @to_do.finished_at = Date.today

    # For time reason, respond_to instead of a custom responder
    respond_to do |format|
      if @to_do.save
        format.html { redirect_to to_dos_path, notice: 'Tarefa finalizada com sucesso' }
      else
        format.html { redirect_to to_dos_path, alert: 'Não foi possível finalizar a tarefa' }
      end
    end
  end

  def destroy
    @to_do.destroy
    respond_with(@to_do, location: -> { to_dos_path })
  end

  private

  def set_to_do
    @to_do = ToDo.find(params[:id])
  end

  def collection
    @params = filter_params
    filter = AppFilter::ToDo.new(@params.merge(user: current_user))
    @to_dos = filter.results
    @to_dos = @to_dos.paginate(page: params[:page])
  end

  def filter_params
    params.permit(:title, :status).delete_if { |_k, v| !v.present? }
  end

  def to_do_params
    params.require(:to_do).permit(:title, :description, :deadline,
                                  :finished_at, :status)
  end
end
