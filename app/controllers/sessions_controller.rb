class SessionsController < ApplicationController
  skip_before_filter :verify_logged_in, only: [:new, :create]

  def new
  end

  def create
    dt = user_params
    user = User.authenticate(dt[:email], dt[:password])

    if user
      session[:user_id] = user.id.to_s
      redirect_to root_url, notice: 'Logado com sucesso'
    else
      flash.now.alert = 'Email ou senha inválido'
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to log_in_path, notice: 'Logout realizado'
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
