require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html, :json

  helper_method :current_user
  before_filter :verify_logged_in

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def verify_logged_in
    unless current_user
      session[:user_id] = nil
      redirect_to log_in_path, alert: 'Faça login para continuar'
    end
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
end
