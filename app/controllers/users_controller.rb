class UsersController < ApplicationController
  skip_before_filter :verify_logged_in, only: [:new, :create]

  def new
    @user = User.new
  end

  def create
    @user = User.new(users_params)
    if @user.save
      session[:user_id] = @user.id.to_s
      redirect_to root_url, notice: 'Bem-vindo!'
    else
      render :new
    end
  end

  private

  def users_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
