$(document).ready ->

  # Initialize the bootstrap's tooltips
  $('.tip').tooltip
    container: 'body'

  # Create jquery masks
  $('.date').mask('00/00/0000')