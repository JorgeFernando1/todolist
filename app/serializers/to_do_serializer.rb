class ToDoSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :deadline, :finished_at, :status

  [:deadline, :finished_at].each do |key|
    define_method key do
      object.send(key) ? I18n.l(object.send(key)) : nil
    end
  end

  def status
    object.status_i18n
  end
end
