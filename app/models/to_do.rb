class ToDo
  include Mongoid::Document
  include Mongoid::Enum
  strip_attributes

  field :title, type: String
  field :description, type: String
  field :deadline, type: Date
  field :finished_at, type: Date
  enum :status, [:pending, :finished], default: :pending

  belongs_to :user

  validates :title, :deadline, presence: true
  validates :deadline, date: { after_or_equal_to: Proc.new { Date.today } }

  scope :find_all, lambda { |user| where(user_id: user.id) }
  scope :find_finished, lambda { |user| where(status: :finished, user_id: user.id)}
  scope :find_pending, lambda { |user| where(status: :pending, user_id: user.id)}
  scope :find_delayed, lambda { |user|
    where(:deadline.lt => Date.today, user_id: user.id)
  }

  def self.icon
    :book
  end
end
