class User
  include Mongoid::Document
  field :email, type: String
  field :password_hash, type: String

  attr_accessor :password

  before_save :encripty_password

  has_many :to_dos

  validates_confirmation_of :password
  validates_presence_of :password, on: :create
  validates_presence_of :email
  validates_uniqueness_of :email

  def encripty_password
    self.password_hash = BCrypt::Password.create(password) if password.present?
  end

  def password_hash
    @password_hash ||= BCrypt::Password.new(self[:password_hash])
  end

  def self.authenticate(email, password)
    user = find_by(email: email)
    return user if user && user.password_hash == password
  end
end
