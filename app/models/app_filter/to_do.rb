module AppFilter
  class ToDo
    include ActiveModel::Model
    attr_accessor :title, :status, :user
    attr_reader :results

    def initialize(attributes = {})
      super(attributes)
      raise 'user can\'t be nil' unless user
      raise 'user aren\'t a User object' if user.class != User
      filter_to_dos
    end

    private

    def filter_to_dos
      @results = ::ToDo.find_all(user)
      filter_by_status if status && ::ToDo::STATUS.include?(status.to_sym)
      filter_delayed if status && status.to_sym == :delayed
      filter_by_title if title && !title.empty?
    end

    def filter_by_status
      @results = @results.where(status: status, user_id: user.id)
    end

    def filter_delayed
      @results = @results.where(:deadline.lt => Date.today, user_id: user.id)
    end

    def filter_by_title
      @results = @results.where(title: /#{title}/i, user_id: user.id)
    end
  end
end
