module ToDosHelper
  def label_status(to_do, status)
    txt = to_do.send("#{status}_i18n")
    # Check if is a delayed to_do
    return content_tag(:span, txt, class: 'label label-danger') if to_do.deadline < Date.today

    case to_do.send(status)
    when :pending then klass = 'label label-warning'
    when :finished then klass = 'label label-success'
    else klass = 'label label-default'
    end
    content_tag(:span, txt, class: klass)
  end
end
