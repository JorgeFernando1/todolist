# Just an another way to create buttons in app
module LinksHelper
  Todo::CONSTANTS::BUTTONS.each do |k, v|
    define_method "button_to_#{k}" do |url, opts = {}|
      opts[:'data-container'] = 'body'
      opts[:class] << v[:btn] if opts[:class]
      opts[:class] ||= v[:btn]
      button_base(k, v[:icon], url, opts)
    end
  end

  def button_base(type, icon, url, opts = {})
    opts[:class] << ' btn tip'
    label = opts.include?(:label) ? opts[:label] : t("buttons.#{type}")
    opts[:title] = label.nil? || label.empty? ? t("buttons.#{type}") : nil
    icon = fa_icon(icon, text: label)
    content_tag(:a, icon, opts.merge(href: url))
  end
end
