# Create form_for fields
module FieldsHelper
  def ftext_field(f, attr, opts = {})
    field_base(f, attr, :text_field, opts)
  end

  def fdate_field(f, attr, opts = {})
    value = f.object.send(attr)
    opts[:value] = value ? I18n.l(value) : nil
    field_base(f, attr, :text_field, opts)
  end

  def farea_field(f, attr, opts = {})
    field_base(f, attr, :text_area, opts)
  end

  def fpassword_field(f, attr, opts = {})
    field_base(f, attr, :password_field, opts)
  end

  def field_base(f, attr, type, opts = {})
    opts[:class] << ' form-control' if opts[:class]
    opts[:class] ||= 'form-control'
    label = f_label(f, attr)
    input = f.send(type, attr, opts)
    content_tag(:div, class: 'form-group') do
      label + input
    end.html_safe
  end

  private

  def f_label(f, attr)
    klass = 'control-label'
    f.label(attr, nil, class: klass)
  end
end
