require 'rails_helper'

RSpec.describe ToDo, type: :model do
  it 'should create a new object' do
    todo = ToDo.new
    expect(todo).to be_a_new(ToDo)
  end

  it 'should build and save a new todo' do
    to_do = build(:to_do)
    to_do.save
    expect(to_do.save).to eq(true)
  end

  it 'shouldnt save if title is empty' do
    to_do = build(:to_do, title: '    ')
    to_do.save
    expect(to_do.errors.count).to eq(1)
    expect(to_do.errors).to include(:title)
  end

  it 'shouldnt save if title is nil' do
    to_do = build(:to_do, title: nil)
    to_do.save
    expect(to_do.errors.count).to eq(1)
    expect(to_do.errors).to include(:title)
  end

  it 'shouldnt save if deadline is nil' do
    to_do = build(:to_do, deadline: nil)
    to_do.save
    expect(to_do.errors.count).to eq(2)
    expect(to_do.errors).to include(:deadline)
  end

  it 'shouldnt save if deadline is before today' do
    to_do = build(:to_do, deadline: 3.days.ago)
    to_do.save
    expect(to_do.errors.count).to eq(1)
    expect(to_do.errors).to include(:deadline)
  end

  it 'should only accept pending and finished as status' do
    to_do = build(:to_do, status: :other)
    to_do.save
    expect(to_do.errors.count).to eq(1)
    expect(to_do.errors).to include(:status)
  end
end
