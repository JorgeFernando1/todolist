require 'rails_helper'

RSpec.describe AppFilter::ToDo, type: :model do
  let(:user) { create(:user) }
  before(:each) { 4.times { create(:to_do, user: user) } }

  context 'when user param' do
    it 'has not passed' do
      expect(-> { AppFilter::ToDo.new }).to raise_error 'user can\'t be nil'
    end

    it 'that is not a User\s instance' do
      expect(-> { AppFilter::ToDo.new(user: 'user') }).to raise_error 'user aren\'t a User object'
    end
  end

  context 'when pass title param' do
    it 'with title exact match' do
      to_do = create(:to_do, title: 'foo-bar', user: user)
      filter = AppFilter::ToDo.new(title: 'foo-bar', user: user)
      expect(filter.results.count).to eq(1)
      expect(filter.results.to_a).to eq([to_do])
    end

    it 'without title exact match' do
      to_do = create(:to_do, title: 'foo-bar', user: user)
      filter = AppFilter::ToDo.new(title:'foo', user: user)
      expect(filter.results.count).to eq(1)
      expect(filter.results.to_a).to eq([to_do])
    end

    it 'without a empty title' do
      filter = AppFilter::ToDo.new(title: '', user: user)
      expect(filter.results.count).to eq(4)
    end
  end

  context 'when pass the status param' do
    it 'with :pending status' do
      filter = AppFilter::ToDo.new(status: :pending, user: user)
      expect(filter.results.count).to eq(4)
    end

    it 'with :finished status' do
      to_do = create(:to_do, status: :finished, user: user)
      filter = AppFilter::ToDo.new(status: :finished, user: user)

      expect(filter.results.count).to eq 1
      expect(filter.results.to_a).to eq [to_do]
    end

    it 'with :another status' do
      create(:to_do, status: :finished, user: user)
      filter = AppFilter::ToDo.new(status: :another, user: user)
      expect(filter.results.count).to eq 5 # Get All to_dos
    end
  end

  context 'find delayed to_dos' do
    before(:each) do
      3.times do
        todo = build(:to_do, deadline: 10.days.ago, user: user)
        todo.save(validate: false)
      end
    end

    it 'lets find them!' do
      filter = AppFilter::ToDo.new(status: :delayed, user: user)
      expect(filter.results.count).to eq(3)
    end
  end

  context 'without params' do
    it 'find all 4 to_dos' do
      filter = AppFilter::ToDo.new(user: user)
      expect(filter.results.count).to eq(4)
    end
  end
end
