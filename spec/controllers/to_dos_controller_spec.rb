require 'rails_helper'

RSpec.describe ToDosController, type: :controller do
  before(:each) { login! }

  describe 'GET #index' do
    it 'respond with an HTTP 200 status' do
      get :index
      expect(response).to have_http_status(200)
    end

    it 'respond with content type equal html' do
      get :index
      expect(response.content_type).to eq('text/html')
    end

    it 'respond with content type equal application/json' do
      get :index, format: :json
      expect(response.content_type).to eq('application/json')
    end

    it 'render the index view' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'contains a single todo' do
      create(:to_do, user: current_user)
      get :index
      expect(assigns(:to_dos).count).to eq(1)
    end
  end

  describe 'GET #new' do
    it 'respond with HTTP status 200' do
      get :new
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'should render new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'respond with text/html content-type' do
      get :new
      expect(response.content_type).to eq('text/html')
    end
  end

  describe 'POST #create' do
    context 'when create successfully' do
      it 'respond with HTTP status 201' do
        post :create, to_do: attributes_for(:to_do)
        expect(response).to have_http_status(:found)
      end

      it 'should redirect_to index when create' do
        post :create, to_do: attributes_for(:to_do)
        expect(response).to redirect_to(to_dos_url)
      end

      it 'have HTTP status created when create model' do
        post :create, to_do: attributes_for(:to_do), format: :json
        expect(response).to have_http_status(:created)
      end

      it 'have content/type as json' do
        post :create, to_do: attributes_for(:to_do), format: :json
        expect(response.content_type).to eq 'application/json'
      end
    end

    context 'when create fails' do
      it 'respond with 200 HTTP status when fail' do
        post :create, to_do: attributes_for(:to_do, title: nil)
        expect(response).to have_http_status(:ok)
      end

      it 'render new template when create fails' do
        post :create, to_do: attributes_for(:to_do, title: nil)
        expect(response).to render_template(:new)
      end

      it 'have HTTP status 422 when create fails' do
        post :create, to_do: attributes_for(:to_do, title: nil), format: :json
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'PUT #update' do
    let(:to_do) { create(:to_do) }

    context 'when update successfully' do
      context 'html #update' do
        it 'render http status found' do
          put :update, id: to_do.id.to_s, to_do: attributes_for(:to_do)
          expect(response).to have_http_status(:found)
        end
        it 'redirect to to_dos_path' do
          put :update, id: to_do.id.to_s, to_do: attributes_for(:to_do)
          expect(response).to redirect_to(to_dos_path)
        end
        it 'content type as text/html' do
          put :update, id: to_do.id.to_s, to_do: attributes_for(:to_do)
          expect(response.content_type).to eq 'text/html'
        end
      end
      context 'json #update' do
        it 'render http status :no_content 204' do
          put :update, id: to_do.id.to_s, to_do: attributes_for(:to_do), format: :json
          expect(response).to have_http_status(:no_content)
        end
        it 'content type as json' do
          put :update, id: to_do.id.to_s, to_do: attributes_for(:to_do), format: :json
          expect(response.content_type).to eq 'application/json'
        end
      end
    end

    context 'when update fails' do
      context 'html #update' do
        it 'have http status 200' do
          put :update, id: to_do.id.to_s,
                       to_do: attributes_for(:to_do, title: nil)
          expect(response).to have_http_status(200)
        end
        it 'render edit template' do
          put :update, id: to_do.id.to_s,
                       to_do: attributes_for(:to_do, title: nil)
          expect(response).to render_template(:edit)
        end
      end
      context 'json #update' do
        it 'have http status 422 (unprocessable entity)' do
          put :update, id: to_do.id.to_s,
                       to_do: attributes_for(:to_do, title: nil), format: :json
          expect(response).to have_http_status(422)
        end
      end
    end
  end

  describe 'GET #finish' do
    let(:to_do) { create(:to_do) }
    context 'when todo finish succesfully' do
      it 'have http_status :found' do
        get :finish, id: to_do.id.to_s
        expect(response).to have_http_status(:found)
      end
      it 'redirect to to_dos_path' do
        get :finish, id: to_do.id.to_s
        expect(response).to redirect_to(to_dos_path)
      end
    end
  end
end
