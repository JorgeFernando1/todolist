FactoryGirl.define do
  factory :to_do do
    title { Faker::Lorem.sentence(4) }
    description { Faker::Lorem.paragraph(2) }
    deadline { Faker::Date.between(Date.today, 3.months.from_now) }
  end
end
