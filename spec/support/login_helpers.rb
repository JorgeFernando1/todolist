module LoginHelpers
  def login!
    user = create(:user)
    request.session[:user_id] = user.id.to_s
  end

  def current_user
    User.find(request.session[:user_id])
  end
end
