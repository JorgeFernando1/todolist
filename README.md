# Novo TodoList

Simples e fácil de usar, como sempre deve ser.

# Mudanças

Atualizado o Rails de 4.1.1 para 4.2.7.1 por causa de um bug com a utilização
de datas como String que ocorria ao criar uma nova tarefa, disparando o seguinte erro "no method error undefined method 'year' for nil:NilClass".

## ToDo Model

Campos:
* title (título): String
* description (descrição): String
* deadline (prazo): Date
* finished_at (finalizado em): Date
* status (status): Enum

Validações: 
* título, prazo e status não podem estar vazios.
* Prazo não pode ser anterior a data atual.


## ToDo Form

* Criação helpers para a construção dos formulários.
* Estilização das mensagens de erros.

## ToDo index

* Filtros para melhor visualização das tarefas.
* Mudanças de cor conforme o status da tarefa.

## Users

Um sistema de autenticação simples, sendo os usuários compostos de email e senha, mas com a lógica principal de autenticação e controle de usuários.


